var jade = require('jade');
var cfg = require('config');
var util = require('util');

module.exports.load = function(name, opts){
    opts = opts || {};

    return jade.renderFile(util.format('%s/%s.jade', cfg.views_dir, name), opts);
};
