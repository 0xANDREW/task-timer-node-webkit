var Backbone = require('backbone');
var $ = require('jquery');
Backbone.$ = $;
var _ = require('underscore');

var loader = require('./jade-loader');
var model = require('./model');

module.exports.MainView = Backbone.View.extend({
    events: {
        'click #clients': 'show_clients'
    },

    initialize: function(){
        this.active_view = null;
    },

    load_view: function(view){
        if (this.active_view){
            if (this.active_view.teardown){
                this.active_view.teardown();
            }

            this.active_view.remove();
        }

        this.active_view = view;
        this.$el.html(this.active_view.render());

        if (this.active_view.post_render){
            this.active_view.post_render();
        }
    },

    render: function(){
        return this.$el.html(loader.load('main'));
    },

    show_clients: function(){
        this.load_view(new ClientListView());

        this.listenTo(this.active_view, 'add-client', this.add_client);
    },

    add_client: function(){
        this.load_view(new AddClientView());

        this.listenTo(this.active_view, 'add-client-cancel', this.show_clients);
    }
});

var ClientListView = Backbone.View.extend({
    events: {
        'click #add': 'add_client',
        'click #delete_all': 'delete_all'
    },

    initialize: function(){
        this.clients = [];
        this.load_clients();
    },

    load_clients: function(){
        model.Client.find({}, function(err, clients){
            this.clients = clients;
            this.render();
        }.bind(this));
    },

    render: function(){
        return this.$el.html(loader.load('clients', { clients: this.clients }));
    },

    add_client: function(){
        this.trigger('add-client');
    },

    delete_all: function(){
        model.Client.find({}, function(err, clients){
            _.each(clients, function(c){
                c.remove();
                this.load_clients();
            }.bind(this));
        }.bind(this));
    }
});

var AddClientView = Backbone.View.extend({
    events: {
        'click #save': 'save',
        'click #cancel': 'cancel'
    },

    render: function(){
        return this.$el.html(loader.load('add_client'));
    },

    save: function(){
        var attrs = {};

        _.each(this.$('.form-control'), function(el){
            attrs[el.id] = $(el).val();
        });

        var c = new model.Client(attrs);

        c.save(function(err, c){
            console.log(c.name);
            this.cancel();
        }.bind(this));
    },
    
    cancel: function(){
        this.trigger('add-client-cancel');
    }
});
