var util = require('util');

var cfg = require('config');
var mongoose = require('mongoose');

mongoose.connect(cfg.mongodb_url);

var Client = mongoose.model('Client', {
    name: String,
    address: String,
    address2: String,
    city: String,
    state: String,
    zip: String,
    phone: String,
    projects: [{
        name: String,
        tasks: [{
            name: String,
            durations: [{
                start: Date,
                end: Date
            }]
        }]
    }]
});

module.exports.Client = Client;
